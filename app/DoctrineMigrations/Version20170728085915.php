<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170728085915 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE catalog_order ADD from_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE catalog_order ADD CONSTRAINT FK_4C3AF22178CED90B FOREIGN KEY (from_id) REFERENCES from_addresses (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_4C3AF22178CED90B ON catalog_order (from_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE catalog_order DROP FOREIGN KEY FK_4C3AF22178CED90B');
        $this->addSql('DROP INDEX IDX_4C3AF22178CED90B ON catalog_order');
        $this->addSql('ALTER TABLE catalog_order DROP from_id');
    }
}
