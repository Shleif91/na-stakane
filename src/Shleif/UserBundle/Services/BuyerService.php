<?php

namespace Shleif\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Shleif\UserBundle\Entity\Buyer;

class BuyerService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $buyerData
     * @return Buyer
     */
    public function createBuyer(array $buyerData)
    {
        if (key_exists('id', $buyerData)) {
            $buyer = $this->em->getRepository('UserBundle:Buyer')
                ->findOneBy([
                    'id' => $buyerData['id']
                ]);
            unset($buyerData['id']);
        } else {
            $buyer = new Buyer();
        }

        foreach ($buyerData as $key => $property) {
            $prop = 'set' . ucfirst($key);
            $buyer->$prop($property);
        }

        $this->em->persist($buyer);
        $this->em->flush();

        return $buyer;
    }
}