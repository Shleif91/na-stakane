<?php

namespace Shleif\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuyerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'ФИО'])
            ->add('phone', null, ['label' => 'Телефон'])
            ->add('metro', null, ['label' => 'Метро'])
            ->add('city', null, ['label' => 'Город'])
            ->add('street', null, ['label' => 'Улица'])
            ->add('house', null, ['label' => 'Дом'])
            ->add('building', null, ['label' => 'Корпус'])
            ->add('staircase', null, ['label' => 'Подъезд'])
            ->add('floor', null, ['label' => 'Этаж'])
            ->add('apartment', null, ['label' => 'Квартира'])
            ->add('intercom', null, ['label' => 'Домофон'])
            ->add('discount', null, ['label' => 'Скидка'])
            ->add('note', null, ['label' => 'Информация'])
            ->add('black', null, ['label' => 'Черный список'])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shleif\UserBundle\Entity\Buyer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shleif_userbundle_buyer';
    }


}
