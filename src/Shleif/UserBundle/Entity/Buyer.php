<?php

namespace Shleif\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Buyer
 *
 * @ORM\Table(name="buyer")
 * @ORM\Entity(repositoryClass="Shleif\UserBundle\Repository\BuyerRepository")
 */
class Buyer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="metro", type="string", length=255, nullable=true)
     */
    private $metro;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", nullable=true)
     */
    private $house;

    /**
     * @var string
     *
     * @ORM\Column(name="building", type="string", nullable=true)
     */
    private $building;

    /**
     * @var string
     *
     * @ORM\Column(name="staircase", type="string", nullable=true)
     */
    private $staircase;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="string", nullable=true)
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="apartment", type="string", nullable=true)
     */
    private $apartment;

    /**
     * @var string
     *
     * @ORM\Column(name="intercom", type="string", length=255, nullable=true)
     */
    private $intercom;

    /**
     * @var int
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @var bool
     *
     * @ORM\Column(name="black", type="boolean")
     */
    private $black;

    /**
     * One Buyer has Many Orders.
     * @ORM\OneToMany(targetEntity="Shleif\CatalogBundle\Entity\CatalogOrder", mappedBy="buyer")
     * @Serializer\Exclude()
     */
    protected $order;

    public function __construct() {
        $this->order = new ArrayCollection();
        $this->discount = 0;
        $this->black = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Buyer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Buyer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set metro
     *
     * @param string $metro
     *
     * @return Buyer
     */
    public function setMetro($metro)
    {
        $this->metro = $metro;

        return $this;
    }

    /**
     * Get metro
     *
     * @return string
     */
    public function getMetro()
    {
        return $this->metro;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Buyer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Buyer
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set house
     *
     * @param integer $house
     *
     * @return Buyer
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return int
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set building
     *
     * @param integer $building
     *
     * @return Buyer
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return int
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set staircase
     *
     * @param integer $staircase
     *
     * @return Buyer
     */
    public function setStaircase($staircase)
    {
        $this->staircase = $staircase;

        return $this;
    }

    /**
     * Get staircase
     *
     * @return int
     */
    public function getStaircase()
    {
        return $this->staircase;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return Buyer
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return int
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set apartment
     *
     * @param integer $apartment
     *
     * @return Buyer
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment
     *
     * @return int
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set intercom
     *
     * @param string $intercom
     *
     * @return Buyer
     */
    public function setIntercom($intercom)
    {
        $this->intercom = $intercom;

        return $this;
    }

    /**
     * Get intercom
     *
     * @return string
     */
    public function getIntercom()
    {
        return $this->intercom;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     *
     * @return Buyer
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Buyer
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set black
     *
     * @param boolean $black
     *
     * @return Buyer
     */
    public function setBlack($black)
    {
        $this->black = $black;

        return $this;
    }

    /**
     * Get black
     *
     * @return bool
     */
    public function getBlack()
    {
        return $this->black;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return Buyer
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }
}

