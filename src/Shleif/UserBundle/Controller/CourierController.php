<?php

namespace Shleif\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shleif\UserBundle\Entity\Courier;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CourierController
 * @package Shleif\UserBundle\Controller
 * @Route("/courier")
 */
class CourierController extends Controller
{
    /**
     * @Route("/", name="courier_show")
     *
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository('UserBundle:Courier')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('UserBundle:Courier:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Creates a new courier entity.
     *
     * @Route("/new", name="courier_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $courier = new Courier();
        $form = $this->createForm('Shleif\UserBundle\Form\CourierType', $courier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($courier);
            $em->flush();

            return $this->redirectToRoute('courier_show', array('id' => $courier->getId()));
        }

        return $this->render('@User/Courier/new.html.twig', array(
            'courier' => $courier,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing courier entity.
     *
     * @Route("/{id}/edit", name="courier_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Courier $courier
     * @return mixed
     */
    public function editAction(Request $request, Courier $courier)
    {
        $editForm = $this->createForm('Shleif\UserBundle\Form\CourierType', $courier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('courier_show');
        }

        return $this->render('@User/Courier/edit.html.twig', array(
            'courier' => $courier,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a courier entity.
     *
     * @Route("/{id}", name="courier_delete")
     * @Method("GET")
     *
     * @param Courier $courier
     * @return mixed
     */
    public function deleteAction(Courier $courier)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($courier);
        $em->flush();

        return $this->redirectToRoute('courier_show');
    }
}
