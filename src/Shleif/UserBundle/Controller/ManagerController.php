<?php

namespace Shleif\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Shleif\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package Shleif\UserBundle\Controller
 *
 * @Security("has_role('ROLE_ADMIN')")
 *
 * @Route("/manager")
 */
class ManagerController extends Controller
{
    /**
     * @Route("/", name="manager_show")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository('UserBundle:User')
            ->findByRole('ROLE_MANAGER');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('UserBundle:Manager:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/new", name="manager_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('Shleif\UserBundle\Form\RegistrationType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_MANAGER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('manager_show', array('id' => $user->getId()));
        }

        return $this->render('@User/Manager/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing courier entity.
     *
     * @Route("/{id}/edit", name="manager_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('Shleif\UserBundle\Form\RegistrationType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('manager_show');
        }

        return $this->render('@User/Manager/edit.html.twig', array(
            'manager' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a courier entity.
     *
     * @Route("/{id}", name="manager_delete")
     * @Method("GET")
     *
     * @param User $user
     * @return mixed
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('manager_show');
    }

    protected function getManagers()
    {
        return $this->getDoctrine()->getRepository('UserBundle:User')->findByRole('ROLE_MANAGER');
    }
}