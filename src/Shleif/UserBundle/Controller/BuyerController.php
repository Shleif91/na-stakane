<?php

namespace Shleif\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shleif\UserBundle\Entity\Buyer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BuyerController
 * @package Shleif\UserBundle\Controller
 * @Route("/buyer")
 */
class BuyerController extends Controller
{
    /**
     * @Route("/", name="buyer_show")
     *
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository('UserBundle:Buyer')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('UserBundle:Buyer:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Creates a new buyer entity.
     *
     * @Route("/new", name="buyer_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $buyer = new Buyer();
        $form = $this->createForm('Shleif\UserBundle\Form\BuyerType', $buyer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buyer);
            $em->flush();

            return $this->redirectToRoute('buyer_show', array('id' => $buyer->getId()));
        }

        return $this->render('@User/Buyer/new.html.twig', array(
            'buyer' => $buyer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing buyer entity.
     *
     * @Route("/{id}/edit", name="buyer_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Buyer $buyer
     * @return mixed
     */
    public function editAction(Request $request, Buyer $buyer)
    {
        $editForm = $this->createForm('Shleif\UserBundle\Form\BuyerType', $buyer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('buyer_show');
        }

        return $this->render('@User/Buyer/edit.html.twig', array(
            'buyer' => $buyer,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a buyer entity.
     *
     * @Route("/{id}", name="buyer_delete")
     * @Method("GET")
     *
     * @param Buyer $buyer
     * @return mixed
     */
    public function deleteAction(Buyer $buyer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($buyer);
        $em->flush();

        return $this->redirectToRoute('buyer_show');
    }
}
