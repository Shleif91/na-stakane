<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Shleif\UserBundle\Entity\Buyer;

class LoadBuyersData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $buyer = new Buyer();
            $buyer->setName($faker->name);
            $buyer->setPhone($faker->phoneNumber);
            $buyer->setCity($faker->city);
            $buyer->setStreet($faker->streetName);
            $buyer->setHouse($faker->buildingNumber);
            $buyer->setBuilding(rand(1, 5));
            $buyer->setFloor(rand(1, 20));
            $buyer->setApartment(rand(1, 200));
            $buyer->setMetro($faker->word);

            $manager->persist($buyer);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}