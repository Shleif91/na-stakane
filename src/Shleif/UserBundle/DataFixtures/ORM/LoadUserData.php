<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Shleif\UserBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $admin = new User();
        $admin->setUsername('admin-0');
        $admin->setName($faker->name);
        $admin->setEmail('admin-0@test.test');
        $admin->setPlainPassword('secret');
        $admin->setRoles(['ROLE_SUPER_ADMIN']);
        $admin->setEnabled(true);
        $manager->persist($admin);

        for ($i = 1; $i < 10; $i++) {
            $admin = new User();
            $admin->setUsername('admin-' . $i);
            $admin->setName($faker->name);
            $admin->setEmail('admin' . $i . '@test.test');
            $admin->setPlainPassword('secret');
            $admin->setRoles(['ROLE_ADMIN']);
            $admin->setEnabled(true);
            $manager->persist($admin);
        }

        for ($i = 0; $i < 10; $i++) {
            $managerU = new User();
            $managerU->setUsername('manager' . $i);
            $managerU->setName($faker->name);
            $managerU->setEmail('manager' . $i .'@test.test');
            $managerU->setPlainPassword('secret');
            $managerU->setRoles(['ROLE_MANAGER']);
            $managerU->setEnabled(true);
            $manager->persist($managerU);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}