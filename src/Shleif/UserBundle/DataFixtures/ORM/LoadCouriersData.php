<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Shleif\UserBundle\Entity\Courier;

class LoadCouriersData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $courier = new Courier();
            $courier->setName($faker->name);
            $courier->setPhone($faker->phoneNumber);
            $courier->setCar($faker->word);
            $courier->setDescription($faker->text(50));

            $manager->persist($courier);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}