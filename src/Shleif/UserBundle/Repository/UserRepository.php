<?php

namespace Shleif\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string $role
     *
     * @return mixed
     */
    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u.name, u.email, u.id, u.username')
            ->from('UserBundle:User', 'u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%"'.$role.'"%');

        return $qb->getQuery();
    }
}