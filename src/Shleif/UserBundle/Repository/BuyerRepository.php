<?php

namespace Shleif\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BuyerRepository
 */
class BuyerRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function getQuery()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('b.id, b.name, b.phone, b.note, b.black')
            ->from('UserBundle:Buyer', 'b');

        return $qb->getQuery();
    }
}
