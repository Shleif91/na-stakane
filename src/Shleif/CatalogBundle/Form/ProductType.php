<?php

namespace Shleif\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Название'])
            ->add('description', null, ['label' => 'Описание'])
            ->add('category', null, ['label' => 'Категория'])
            ->add('balance', null, ['label' => 'Наличие'])
            ->add('cost', null, ['label' => 'Рыночная стоимость'])
            ->add('purchase_cost', null, ['label' => 'Закупочная стоимость']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shleif\CatalogBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shleif_catalogbundle_product';
    }


}
