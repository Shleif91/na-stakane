<?php

namespace Shleif\CatalogBundle\Form;

use Shleif\CatalogBundle\Repository\ProductRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $order = $options['data']->getOrder();

        if ($options['flag'] == 'new') {
            $builder->add('order', null, ['disabled' => true])
                ->add('product', EntityType::class, [
                    'class' => 'Shleif\CatalogBundle\Entity\Product',
                    'query_builder' => function (ProductRepository $repository) use ($order) {
                        return $repository->findNotIssetProductsByOrder($order);
                    },
                    'choice_label' => 'name',
                    'expanded' => false,
                    'multiple' => false
                ])
                ->add('number');
        } else {
            $builder->add('order', null, ['disabled' => true])
                ->add('product', null, ['disabled' => true])
                ->add('number');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shleif\CatalogBundle\Entity\OrderProduct',
            'flag' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shleif_catalogbundle_orderproduct';
    }
}
