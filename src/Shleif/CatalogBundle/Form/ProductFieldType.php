<?php

namespace Shleif\CatalogBundle\Form;

use Shleif\CatalogBundle\Repository\FieldRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFieldType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $product = $options['data']->getProduct();

        if ($options['flag'] == 'new') {
            $builder
                ->add('product', null, [
                    'disabled' => true,
                    'label' => 'Товар'
                ])
                ->add('field', EntityType::class, [
                    'class' => 'Shleif\CatalogBundle\Entity\Field',
                    'query_builder' => function (FieldRepository $repository) use ($product) {
                        return $repository->findNotIssetFieldsByProduct($product);
                    },
                    'choice_label' => 'title',
                    'expanded' => false,
                    'multiple' => false,
                    'label' => 'Свойство'
                ])
                ->add('value', null, [
                    'label' => 'Значение'
                ]);
        } else {
            $builder
                ->add('product', null, [
                    'disabled' => true,
                    'label' => 'Товар'
                ])
                ->add('field', null, [
                    'disabled' => true,
                    'label' => 'Свойство'
                ])
                ->add('value', null, [
                    'label' => 'Значение'
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shleif\CatalogBundle\Entity\ProductField',
            'flag' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shleif_catalogbundle_productfield';
    }
}
