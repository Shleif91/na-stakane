<?php

namespace Shleif\CatalogBundle\Event;

use Shleif\CatalogBundle\Entity\Product;

class CreateProductEvent extends ProductEvent
{
    const EVENT_NAME = 'CreateProductEvent';

    /**
     * CreateProductEvent constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        parent::__construct($product);
    }
}