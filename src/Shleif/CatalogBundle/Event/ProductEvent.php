<?php

namespace Shleif\CatalogBundle\Event;

use Shleif\CatalogBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductEvent extends Event
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductEvent constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}