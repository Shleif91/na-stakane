<?php

namespace Shleif\CatalogBundle\Event\Subscriber;

use Shleif\CatalogBundle\Event\CreateProductEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateProductSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            CreateProductEvent::EVENT_NAME => 'onCreateProduct'
        ];
    }

    public function onCreateProduct(CreateProductEvent $event)
    {
        $product = $event->getProduct();
    }
}