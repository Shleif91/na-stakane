<?php

namespace Shleif\CatalogBundle\Services;

use Doctrine\ORM\EntityManager;
use Shleif\CatalogBundle\Entity\CatalogOrder;
use Shleif\CatalogBundle\Entity\OrderProduct;
use Shleif\CatalogBundle\Entity\Product;

class OrderProductService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param CatalogOrder $order
     * @param array $products
     */
    public function linkOrderAndProducts(CatalogOrder $order, array $products)
    {
        $obsoleteData = $this->em->getRepository('CatalogBundle:OrderProduct')->findBy([
            'order' => $order
        ]);

        foreach ($products as $product) {
            $findProduct = $this->em->getRepository('CatalogBundle:Product')
                ->findOneBy(['id' => $product['product']['id']]);

            $orderProduct = $this->em->getRepository('CatalogBundle:OrderProduct')
                ->findOneBy([
                    'order' => $order,
                    'product' => $findProduct
                ]);

            if (!$orderProduct) {
                $orderProduct = new OrderProduct();
                $orderProduct->setOrder($order);
                $orderProduct->setProduct($findProduct);
            } else {
                $obsoleteData = $this->clearOrderProductData($obsoleteData, $findProduct);
            }

            $balance = $findProduct->getBalance() - ($product['value'] - $orderProduct->getNumber());
            $findProduct->setBalance($balance);
            $orderProduct->setNumber($product['value']);

            $this->em->persist($orderProduct);
            $this->em->persist($findProduct);
        }

        foreach ($obsoleteData as $data) {
            $product = $data->getProduct();
            $product->setBalance($product->getBalance() + $data->getNumber());
            $this->em->remove($data);
        }

        $this->em->flush();
    }

    /**
     * @param OrderProduct[] $obsoleteData
     * @param Product $product
     *
     * @return array
     */
    private function clearOrderProductData(array $obsoleteData, Product $product)
    {
        foreach ($obsoleteData as $key => $data) {
            if ($data->getProduct() === $product) {
                unset($obsoleteData[$key]);
            }
        }

        return $obsoleteData;
    }
}