<?php

namespace Shleif\CatalogBundle\Services;

use Doctrine\ORM\EntityManager;
use Shleif\CatalogBundle\Entity\CatalogOrder;
use Shleif\UserBundle\Entity\Buyer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CatalogOrderService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $data
     * @param Buyer $buyer
     *
     * @return CatalogOrder;
     */
    public function createOrder(array $data, Buyer $buyer)
    {
        if ($data['id']) {
            $order = $this->em->getRepository('CatalogBundle:CatalogOrder')->findOneBy([
                'id' => $data['id']
            ]);
        } else {
            $order = new CatalogOrder();
        }
        unset($data['id']);

        $order->setBuyer($buyer);

        if (!key_exists('manager', $data) || $data['manager'] == '') {
            throw new BadRequestHttpException('Not set manager');
        }

        $manager = $this->em->getRepository('UserBundle:User')->findOneBy(['id' => $data['manager']]);
        $order->setManager($manager);
        unset($data['manager']);

        if (!key_exists('courier', $data) || $data['courier'] == '') {
            throw new BadRequestHttpException('Not set courier');
        }

        $courier = $this->em->getRepository('UserBundle:Courier')->findOneBy(['id' => $data['courier']]);
        $order->setCourier($courier);
        unset($data['courier']);

        if (!key_exists('status', $data) || $data['status'] == '') {
            throw new BadRequestHttpException('Not set status');
        }

        $status = $this->em->getRepository('CatalogBundle:Status')->findOneBy(['id' => $data['status']]);
        $order->setStatus($status);
        unset($data['status']);

        if (!key_exists('payment', $data) || $data['payment'] == '') {
            throw new BadRequestHttpException('Not set payment');
        }

        $payment = $this->em->getRepository('CatalogBundle:Payment')->findOneBy(['id' => $data['payment']]);
        $order->setPayment($payment);
        unset($data['payment']);

        if (!key_exists('from', $data) || $data['from'] == '') {
            throw new BadRequestHttpException('Not set from');
        }

        $from = $this->em->getRepository('CatalogBundle:FromAddress')->findOneBy(['id' => $data['from']]);
        $order->setFrom($from);
        unset($data['from']);

        if (!key_exists('date', $data) || $data['date'] == '') {
            throw new BadRequestHttpException('Not set payment');
        }

        $order->setDate(new \DateTime($data['date']));
        unset($data['date']);

        foreach ($data as $key => $property) {
            $prop = 'set' . ucfirst($key);
            $order->$prop($property);
        }

        $this->em->persist($order);
        $this->em->flush();

        return $order;
    }
}
