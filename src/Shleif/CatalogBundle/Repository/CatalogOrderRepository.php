<?php

namespace Shleif\CatalogBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;

/**
 * CatalogOrderRepository
 */
class CatalogOrderRepository extends EntityRepository
{
    public function findWithPaginate($page = 1, $perPage = 5, $filters)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from('CatalogBundle:CatalogOrder', 'c')
            ->orderBy('c.id', 'DESC');

        $this->addDateFilter($qb, $filters);
        unset($filters['dates']);

        $this->addFilterByBuyerPhone($qb, $filters);
        unset($filters['buyerPhone']);

        $this->addOverdueFilter($qb, $filters);
        unset($filters['overdue']);

        $this->freeDeliveryFilter($qb, $filters);
        unset($filters['freeDelivery']);

        $this->addOrderNumberFilter($qb, $filters);
        unset($filters['numberOrder']);

        $this->addFilterByProductName($qb, $filters);
        unset($filters['productName']);

        foreach ($filters as $key => $filter) {
            if ($filter != '') {
                $qb->andWhere('c.' . $key . ' = ' . $filter);
            }
        }

        $qb2 = clone $qb;

        $qb->select('c');
        $qb2->select('COUNT(c)');

        $qb->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        $data = $qb->getQuery()->getResult();
        $count = $qb2->getQuery()->getSingleScalarResult();

        $total = $this->_em->createQueryBuilder()
            ->select('COUNT(c)')
            ->from('CatalogBundle:CatalogOrder', 'c')
            ->getQuery()
            ->getSingleScalarResult();

        return [
            'data' => $data,
            'total' => $total,
            'count' => $count,
            'costs' => $this->getCostsOrders()
        ];
    }

    /**
     * @param QueryBuilder $qb
     * @param $filters
     */
    protected function addDateFilter(QueryBuilder $qb, $filters)
    {
        if (!empty($filters['dates']['from'])) {
            try {
                $from = new \DateTime($filters['dates']['from']);
                $qb->andWhere('c.createdAt >= :dateFrom')
                    ->setParameter('dateFrom', $from, Type::DATETIME);
            } catch (Exception $exception) {}
        }
        if (!empty($filters['dates']['to'])) {
            try {
                $to = new \DateTime($filters['dates']['to']);
                $qb->andWhere('c.createdAt <= :dateTo')
                    ->setParameter('dateTo', $to, Type::DATETIME);
            } catch (Exception $exception) {}
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param $filters
     */
    protected function addFilterByBuyerPhone(QueryBuilder $qb, $filters)
    {
        if (!empty($filters['buyerPhone'])) {
            $qb->join('c.buyer', 'b')
                ->andWhere('b.phone LIKE :phone')
                ->setParameter('phone', '%'.$filters['buyerPhone'].'%');
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param $filters
     */
    protected function addFilterByProductName(QueryBuilder $qb, $filters)
    {
        if (!empty($filters['productName'])) {
            $qb->join('c.product', 'op')
                ->join('op.product', 'p')
                ->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.$filters['productName'].'%');
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filters
     */
    protected function addOverdueFilter(QueryBuilder $qb, $filters = [])
    {
        if (!empty($filters['overdue'])) {
            $now = new \DateTime();
            $qb->andWhere('c.date < :dateTo')
                ->setParameter('dateTo', $now, Type::DATETIME);
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filters
     */
    protected function freeDeliveryFilter(QueryBuilder $qb, $filters = [])
    {
        if ($filters['freeDelivery']) {
            $qb->andWhere('c.delivery = 0');
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filters
     */
    protected function addOrderNumberFilter(QueryBuilder $qb, $filters = [])
    {
        if ($filters['numberOrder']) {
            $qb->andWhere('c.id LIKE :id')
                ->setParameter('id', '%'.$filters['numberOrder'].'%');
        }
    }

    /**
     * @return array|null
     */
    protected function getCostsOrders()
    {
        $cost = $this->_em->createQueryBuilder()
            ->from('CatalogBundle:CatalogOrder', 'o')
            ->join('o.product', 'op')
            ->join('op.product', 'p')
            ->select('SUM(p.cost * op.number) as productsCost, SUM(p.purchaseCost * op.number) as purchaseCost')
            ->getQuery()
            ->getResult();

        return $cost[0];
    }
}
