<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Shleif\CatalogBundle\Entity\OrderStatus;

class LoadOrderStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $orderStatus = new OrderStatus();
        $orderStatus->setTitle('Новый');
        $manager->persist($orderStatus);
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}