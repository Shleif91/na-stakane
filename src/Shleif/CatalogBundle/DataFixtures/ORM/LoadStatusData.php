<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Shleif\CatalogBundle\Entity\Status;

class LoadStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $status = new Status();
        $status->setTitle('Новый');
        $manager->persist($status);

        $status = new Status();
        $status->setTitle('Выполнен');
        $manager->persist($status);

        $status = new Status();
        $status->setTitle('Отменен');
        $manager->persist($status);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}