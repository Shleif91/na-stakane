<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Shleif\CatalogBundle\Entity\Product;

class LoadProductsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $categories = $manager->getRepository('CatalogBundle:Category')->findAll();

        foreach ($categories as $category) {
            for ($i = 0; $i < 10; $i++) {
                $product = new Product();
                $product->setCategory($category);
                $product->setName($faker->unique()->word);
                $product->setDescription($faker->text(100));
                $product->setBalance(rand(1, 10));
                $product->setCost(rand(500, 2000));
                $product->setPurchaseCost($product->getCost() - 100);

                $manager->persist($product);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}