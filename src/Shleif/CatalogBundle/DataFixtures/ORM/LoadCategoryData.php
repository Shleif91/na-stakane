<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Shleif\CatalogBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('Пиво');
        $category->setDescription('Категория пива');
        $manager->persist($category);

        $category = new Category();
        $category->setName('Водка');
        $category->setDescription('Категория водки');
        $manager->persist($category);

        $category = new Category();
        $category->setName('Коньяк');
        $category->setDescription('Категория коньяков');
        $manager->persist($category);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}