<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Shleif\CatalogBundle\Entity\FromAddress;

class LoadFromAddressData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $from = new FromAddress();
        $from->setCity('Москва');
        $from->setStreet('ул. Вавилова');
        $from->setHouse('81');
        $from->setBuilding(null);
        $manager->persist($from);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}