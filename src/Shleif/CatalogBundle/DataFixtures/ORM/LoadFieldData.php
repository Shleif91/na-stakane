<?php

namespace Shleif\CatalogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Shleif\CatalogBundle\Entity\Field;

class LoadFieldData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $field = new Field();
        $field->setTitle('Крепость');
        $manager->persist($field);

        $field = new Field();
        $field->setTitle('Объём');
        $manager->persist($field);

        $field = new Field();
        $field->setTitle('Цвет');
        $manager->persist($field);


        $field = new Field();
        $field->setTitle('Сладость');
        $manager->persist($field);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}