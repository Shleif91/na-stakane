<?php

namespace Shleif\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="instructions")
 * @ORM\Entity(repositoryClass="Shleif\CatalogBundle\Repository\InstructionRepository")
 */
class Instruction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return Instruction
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
}