<?php

namespace Shleif\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Shleif\UserBundle\Entity\Courier;
use Shleif\UserBundle\Entity\User;

/**
 * CatalogOrder
 *
 * @ORM\Table(name="catalog_order")
 * @ORM\Entity(repositoryClass="Shleif\CatalogBundle\Repository\CatalogOrderRepository")
 */
class CatalogOrder
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * One Order has Many OrderProduct.
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Type("DateTime<'d/m/Y H:i:s'>")
     */
    private $date;

    /**
     * Many Orders have One Manager.
     *
     * @ORM\ManyToOne(targetEntity="Shleif\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $manager;

    /**
     * Many Orders have One Courier.
     *
     * @ORM\ManyToOne(targetEntity="Shleif\UserBundle\Entity\Courier")
     * @ORM\JoinColumn(name="courier_id", referencedColumnName="id")
     */
    private $courier;

    /**
     * Many Orders have One Statuses.
     *
     * @ORM\ManyToOne(targetEntity="Shleif\CatalogBundle\Entity\Status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * Many Orders have One Payments.
     *
     * @ORM\ManyToOne(targetEntity="Shleif\CatalogBundle\Entity\Payment")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private $payment;

    /**
     * @var integer
     * @ORM\Column(name="delivery", type="integer")
     */
    private $delivery;

    /**
     * @var integer
     * @ORM\Column(name="cost", type="integer")
     */
    protected $cost;

    /**
     * @var string
     * @ORM\Column(name="lead_time", type="string", nullable=true)
     */
    protected $leadTime;

    /**
     * @var integer
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    protected $discount;

    /**
     * @var string
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    protected $note;

    /**
     * @var boolean
     * @ORM\Column(name="call_at_gate", type="boolean")
     */
    protected $callAtGate;

    /**
     * @var boolean
     * @ORM\Column(name="send_sms", type="boolean")
     */
    protected $sendSms;

    /**
     * @var integer
     * @ORM\Column(name="banknote", type="integer")
     */
    protected $banknote;

    /**
     * Many Orders have One Buyer.
     * @ORM\ManyToOne(targetEntity="Shleif\UserBundle\Entity\Buyer", inversedBy="order")
     * @ORM\JoinColumn(name="buyer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $buyer;

    /**
     * Many Orders have One FromAddress.
     * @ORM\ManyToOne(targetEntity="Shleif\CatalogBundle\Entity\FromAddress")
     * @ORM\JoinColumn(name="from_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $from;

    /**
     * CatalogOrder constructor.
     */
    public function __construct() {
        $this->product = new ArrayCollection();
        $this->delivery = 0;
    }

    /**
     * @return int
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param int $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return CatalogOrder
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set manager
     *
     * @param User $manager
     * @return CatalogOrder
     */
    public function setManager(User $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set courier
     *
     * @param Courier $courier
     * @return CatalogOrder
     */
    public function setCourier($courier)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return Courier
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Set status
     *
     * @param Status $status
     * @return CatalogOrder
     */
    public function setStatus(Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set payment
     *
     * @param Payment $payment
     * @return CatalogOrder
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     * @return CatalogOrder
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return string
     */
    public function getLeadTime()
    {
        return $this->leadTime;
    }

    /**
     * @param string $leadTime
     * @return CatalogOrder
     */
    public function setLeadTime($leadTime)
    {
        $this->leadTime = $leadTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     * @return CatalogOrder
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return CatalogOrder
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCallAtGate()
    {
        return $this->callAtGate;
    }

    /**
     * @param bool $callAtGate
     * @return CatalogOrder
     */
    public function setCallAtGate($callAtGate)
    {
        $this->callAtGate = $callAtGate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendSms()
    {
        return $this->sendSms;
    }

    /**
     * @param bool $sendSms
     * @return CatalogOrder
     */
    public function setSendSms($sendSms)
    {
        $this->sendSms = $sendSms;

        return $this;
    }

    /**
     * @return int
     */
    public function getBanknote()
    {
        return $this->banknote;
    }

    /**
     * @param int $banknote
     * @return CatalogOrder
     */
    public function setBanknote($banknote)
    {
        $this->banknote = $banknote;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param mixed $buyer
     * @return CatalogOrder
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    public function __toString()
    {
        return '#'.$this->id . ' ' . $this->date->format('Y-m-d');
    }

    /**
     * @return FromAddress
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param FromAddress $from
     * @return CatalogOrder
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }
}
