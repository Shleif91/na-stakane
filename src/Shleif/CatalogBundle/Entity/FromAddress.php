<?php

namespace Shleif\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldController
 *
 * @ORM\Table(name="from_addresses")
 * @ORM\Entity(repositoryClass="Shleif\CatalogBundle\Repository\FromAddressRepository")
 */
class FromAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="city")
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="street")
     */
    protected $street;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="house")
     */
    protected $house;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="building", nullable=true)
     */
    protected $building;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return FromAddress
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return FromAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     * @return FromAddress
     */
    public function setHouse($house)
    {
        $this->house = $house;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param string $building
     * @return FromAddress
     */
    public function setBuilding($building)
    {
        $this->building = $building;
        return $this;
    }
}