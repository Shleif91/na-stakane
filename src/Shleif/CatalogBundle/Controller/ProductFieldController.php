<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Shleif\CatalogBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\ProductField;

/**
 * Class ProductController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/product")
 */
class ProductFieldController extends Controller
{
    /**
     * @Route("/{id}/show-fields", name="product_field_show")
     * @param Product $product
     * @return mixed
     */
    public function indexFieldAction(Product $product)
    {
        $repository = $this->getDoctrine()->getRepository('CatalogBundle:ProductField');
        $fields = $repository->findAllByProduct($product)->getResult();

        return $this->render('@Catalog/ProductField/index.html.twig', [
            'fields' => $fields,
            'product' => $product
        ]);
    }

    /**
     * Creates a new product field entity.
     *
     * @Route("/{id}/new-field", name="product_field_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    public function newAction(Request $request, Product $product)
    {
        $productField = new ProductField();
        $productField->setProduct($product);
        $form = $this->createForm('Shleif\CatalogBundle\Form\ProductFieldType', $productField, ['flag' => 'new']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($productField);
            $em->flush();

            return $this->redirectToRoute('product_field_show', [
                'id' => $product->getId()
            ]);
        }

        return $this->render('@Catalog/ProductField/new.html.twig', [
            'productField' => $productField,
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing product field entity.
     *
     * @Route("/{prod_id}/edit-field/{field_id}", name="product_field_edit")
     * @ParamConverter("product", class="CatalogBundle:Product", options={"id" = "prod_id"})
     * @ParamConverter("productField", class="CatalogBundle:ProductField", options={"id" = "field_id"})
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Product $product
     * @param ProductField $productField
     * @return mixed
     */
    public function editAction(Request $request, Product $product, ProductField $productField)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\ProductFieldType', $productField, ['flag' => 'edit']);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_field_show', [
                'id' => $product->getId()
            ]);
        }

        return $this->render('@Catalog/ProductField/edit.html.twig', [
            'productField' => $productField,
            'product' => $product,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Deletes a product field entity.
     *
     * @Route("/{prod_id}/field-delete/{field_id}", name="product_field_delete")
     * @ParamConverter("product", class="CatalogBundle:Product", options={"id" = "prod_id"})
     * @ParamConverter("productField", class="CatalogBundle:ProductField", options={"id" = "field_id"})
     * @Method("GET")
     *
     * @param Product $product
     * @param ProductField $productField
     * @return mixed
     */
    public function deleteAction(Product $product, ProductField $productField)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($productField);
        $em->flush();

        return $this->redirectToRoute('product_field_show', [
            'id' => $product->getId()
        ]);
    }
}
