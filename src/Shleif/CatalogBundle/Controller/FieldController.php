<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\Field;

/**
 * Class FieldController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/field")
 */
class FieldController extends Controller
{
    /**
     * @Route("/", name="field_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fields = $em->getRepository('CatalogBundle:Field')->findAll();

        return $this->render('@Catalog/Field/index.html.twig', array(
            'fields' => $fields,
        ));
    }

    /**
     * Creates a new field entity.
     *
     * @Route("/new", name="field_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $field = new Field();
        $form = $this->createForm('Shleif\CatalogBundle\Form\FieldType', $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($field);
            $em->flush();

            return $this->redirectToRoute('field_show', array('id' => $field->getId()));
        }

        return $this->render('@Catalog/Field/new.html.twig', array(
            'field' => $field,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing field entity.
     *
     * @Route("/{id}/edit", name="field_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Field $field
     * @return mixed
     */
    public function editAction(Request $request, Field $field)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\FieldType', $field);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('field_show');
        }

        return $this->render('@Catalog/Field/edit.html.twig', array(
            'field' => $field,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a field entity.
     *
     * @Route("/{id}", name="field_delete")
     * @Method("GET")
     *
     * @param Field $field
     * @return mixed
     */
    public function deleteAction(Field $field)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($field);
        $em->flush();

        return $this->redirectToRoute('field_show');
    }
}
