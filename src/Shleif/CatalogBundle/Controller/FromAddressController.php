<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shleif\CatalogBundle\Entity\FromAddress;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FromAddressController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/from-address")
 */
class FromAddressController extends Controller
{
    /**
     * @Route("/", name="from_address_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fromAddresses = $em->getRepository('CatalogBundle:FromAddress')->findAll();

        return $this->render('@Catalog/FromAddress/index.html.twig', array(
            'fromAddresses' => $fromAddresses,
        ));
    }

    /**
     * Creates a new FromAddress entity.
     *
     * @Route("/new", name="from_address_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $fromAddress = new FromAddress();
        $form = $this->createForm('Shleif\CatalogBundle\Form\FromAddressType', $fromAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fromAddress);
            $em->flush();

            return $this->redirectToRoute('from_address_show', array('id' => $fromAddress->getId()));
        }

        return $this->render('@Catalog/FromAddress/new.html.twig', array(
            'fromAddress' => $fromAddress,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fromAddress entity.
     *
     * @Route("/{id}/edit", name="from_address_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FromAddress $fromAddress
     * @return mixed
     */
    public function editAction(Request $request, FromAddress $fromAddress)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\FromAddressType', $fromAddress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('from_address_show');
        }

        return $this->render('@Catalog/FromAddress/edit.html.twig', array(
            'fromAddress' => $fromAddress,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a fromAddress entity.
     *
     * @Route("/{id}", name="from_address_delete")
     * @Method("GET")
     *
     * @param FromAddress $fromAddress
     * @return mixed
     */
    public function deleteAction(FromAddress $fromAddress)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($fromAddress);
        $em->flush();

        return $this->redirectToRoute('from_address_show');
    }
}