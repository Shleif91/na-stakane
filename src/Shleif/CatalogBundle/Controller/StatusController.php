<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\Status;

/**
 * Class StatusController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/status")
 */
class StatusController extends Controller
{
    /**
     * @Route("/", name="status_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $statuses = $em->getRepository('CatalogBundle:Status')->findAll();

        return $this->render('@Catalog/Status/index.html.twig', array(
            'statuses' => $statuses,
        ));
    }

    /**
     * Creates a new status entity.
     *
     * @Route("/new", name="status_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $status = new Status();
        $form = $this->createForm('Shleif\CatalogBundle\Form\StatusType', $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($status);
            $em->flush();

            return $this->redirectToRoute('status_show', array('id' => $status->getId()));
        }

        return $this->render('@Catalog/Status/new.html.twig', array(
            'status' => $status,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing status entity.
     *
     * @Route("/{id}/edit", name="status_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Status $status
     * @return mixed
     */
    public function editAction(Request $request, Status $status)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\StatusType', $status);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('status_show');
        }

        return $this->render('@Catalog/Status/edit.html.twig', array(
            'status' => $status,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a status entity.
     *
     * @Route("/{id}", name="status_delete")
     * @Method("GET")
     *
     * @param Status $status
     * @return mixed
     */
    public function deleteAction(Status $status)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($status);
        $em->flush();

        return $this->redirectToRoute('status_show');
    }
}
