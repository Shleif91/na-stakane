<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shleif\CatalogBundle\Entity\CatalogOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CatalogOrderController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/order")
 */
class CatalogOrderController extends Controller
{
    /**
     * @Route("/", name="order_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $orders = $em->getRepository('CatalogBundle:CatalogOrder')->findAll();

        return $this->render('@Catalog/CatalogOrder/index.html.twig', array(
            'orders' => $orders,
        ));
    }

    /**
     * Creates a new order entity.
     *
     * @Route("/new", name="order_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $order = new CatalogOrder();
        $form = $this->createForm('Shleif\CatalogBundle\Form\CatalogOrderType', $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            return $this->redirectToRoute('order_show', array('id' => $order->getId()));
        }

        return $this->render('@Catalog/CatalogOrder/new.html.twig', array(
            'order' => $order,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing order entity.
     *
     * @Route("/{id}/edit", name="order_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param CatalogOrder $order
     * @return mixed
     */
    public function editAction(Request $request, CatalogOrder $order)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\CatalogOrderType', $order);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_show');
        }

        return $this->render('@Catalog/CatalogOrder/edit.html.twig', array(
            'order' => $order,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a order entity.
     *
     * @Route("/{id}", name="order_delete")
     * @Method("GET")
     *
     * @param CatalogOrder $order
     * @return mixed
     */
    public function deleteAction(CatalogOrder $order)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();

        return $this->redirectToRoute('order_show');
    }
}
