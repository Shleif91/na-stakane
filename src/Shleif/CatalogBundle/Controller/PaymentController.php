<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\Payment;

/**
 * Class PaymentController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/", name="payment_show")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payments = $em->getRepository('CatalogBundle:Payment')->findAll();

        return $this->render('@Catalog/Payment/index.html.twig', array(
            'payments' => $payments,
        ));
    }

    /**
     * Creates a new payment entity.
     *
     * @Route("/new", name="payment_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $payment = new Payment();
        $form = $this->createForm('Shleif\CatalogBundle\Form\PaymentType', $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            return $this->redirectToRoute('payment_show', array('id' => $payment->getId()));
        }

        return $this->render('@Catalog/Payment/new.html.twig', array(
            'payment' => $payment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing payment entity.
     *
     * @Route("/{id}/edit", name="payment_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Payment $payment
     * @return mixed
     */
    public function editAction(Request $request, Payment $payment)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\PaymentType', $payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('payment_show');
        }

        return $this->render('@Catalog/Payment/edit.html.twig', array(
            'payment' => $payment,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a payment entity.
     *
     * @Route("/{id}", name="payment_delete")
     * @Method("GET")
     *
     * @param Payment $payment
     * @return mixed
     */
    public function deleteAction(Payment $payment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($payment);
        $em->flush();

        return $this->redirectToRoute('payment_show');
    }
}
