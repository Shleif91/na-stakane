<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Shleif\CatalogBundle\Entity\CatalogOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\OrderProduct;

/**
 * Class OrderProductController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/order")
 */
class OrderProductController extends Controller
{
    /**
     * @Route("/{id}/show-products", name="order_product_show")
     * @param CatalogOrder $order
     * @return mixed
     */
    public function indexFieldAction(CatalogOrder $order)
    {
        $repository = $this->getDoctrine()->getRepository('CatalogBundle:OrderProduct');
        $products = $repository->findAllByCatalogOrder($order)->getResult();

        return $this->render('@Catalog/OrderProduct/index.html.twig', [
            'products' => $products,
            'order' => $order
        ]);
    }

    /**
     * Creates a new order product entity.
     *
     * @Route("/{id}/new-product", name="order_product_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param CatalogOrder $order
     * @return mixed
     */
    public function newAction(Request $request, CatalogOrder $order)
    {
        $orderProduct = new OrderProduct();
        $orderProduct->setOrder($order);
        $form = $this->createForm('Shleif\CatalogBundle\Form\OrderProductType', $orderProduct, ['flag' => 'new']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderProduct);
            $em->flush();

            return $this->redirectToRoute('order_product_show', [
                'id' => $order->getId()
            ]);
        }

        return $this->render('@Catalog/OrderProduct/new.html.twig', [
            'orderProduct' => $orderProduct,
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing order product entity.
     *
     * @Route("/{order_id}/edit-field/{product_id}", name="order_product_edit")
     * @ParamConverter("order", class="CatalogBundle:CatalogOrder", options={"id" = "order_id"})
     * @ParamConverter("orderProduct", class="CatalogBundle:OrderProduct", options={"id" = "product_id"})
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param CatalogOrder $order
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function editAction(Request $request, CatalogOrder $order, OrderProduct $orderProduct)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\OrderProductType', $orderProduct, ['flag' => 'edit']);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_product_show', [
                'id' => $order->getId()
            ]);
        }

        return $this->render('@Catalog/OrderProduct/edit.html.twig', [
            'orderProduct' => $orderProduct,
            'order' => $order,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Deletes a order product entity.
     *
     * @Route("/{order_id}/field-delete/{product_id}", name="order_product_delete")
     * @ParamConverter("order", class="CatalogBundle:CatalogOrder", options={"id" = "order_id"})
     * @ParamConverter("orderProduct", class="CatalogBundle:OrderProduct", options={"id" = "product_id"})
     * @Method("GET")
     *
     * @param CatalogOrder $order
     * @param OrderProduct $orderProduct
     * @return mixed
     */
    public function deleteAction(CatalogOrder $order, OrderProduct $orderProduct)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($orderProduct);
        $em->flush();

        return $this->redirectToRoute('order_product_show', [
            'id' => $order->getId()
        ]);
    }
}
