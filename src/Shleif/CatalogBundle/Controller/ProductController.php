<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Shleif\CatalogBundle\Event\CreateProductEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Shleif\CatalogBundle\Entity\Product;

/**
 * Class ProductController
 * @package Shleif\CatalogBundle\Controller
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="product_show")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository('CatalogBundle:Product')
            ->createQueryBuilder('p')
            ->join('p.category','category')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('@Catalog/Product/index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="product_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('Shleif\CatalogBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $event = new CreateProductEvent($product);
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(CreateProductEvent::EVENT_NAME, $event);

            return $this->redirectToRoute('product_show', array('id' => $product->getId()));
        }

        return $this->render('@Catalog/Product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="product_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    public function editAction(Request $request, Product $product)
    {
        $editForm = $this->createForm('Shleif\CatalogBundle\Form\ProductType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_show');
        }

        return $this->render('@Catalog/Product/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="product_delete")
     * @Method("GET")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param Product $product
     * @return mixed
     */
    public function deleteAction(Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('product_show');
    }
}
