<?php

namespace Shleif\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    public function sidebarAction()
    {
        $instruction = $this->getDoctrine()->getRepository('CatalogBundle:Instruction')->findOneBy([]);

        return $this->render('CatalogBundle:Sidebar:index.html.twig', [
            'instruction' => $instruction
        ]);
    }
}