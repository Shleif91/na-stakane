<?php

namespace Shleif\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Shleif\CatalogBundle\Entity\Instruction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InstructionController
 * @package Shleif\CatalogBundle\Controller
 *
 * @Security("has_role('ROLE_ADMIN')")
 */
class InstructionController extends Controller
{
    /**
     * @Route("/instruction", name="action_instruction")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $instruction = $this->getDoctrine()->getRepository('CatalogBundle:Instruction')->findOneBy([]);

        if (!$instruction) {
            $instruction = new Instruction();
        }

        $form = $this->createForm('Shleif\CatalogBundle\Form\InstructionType', $instruction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($instruction);
            $em->flush();
        }

        return $this->render('@Catalog/Instruction/index.html.twig', array(
            'field' => $instruction,
            'form' => $form->createView(),
        ));
    }
}