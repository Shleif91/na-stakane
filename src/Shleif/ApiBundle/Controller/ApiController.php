<?php

namespace Shleif\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Shleif\CatalogBundle\Entity\CatalogOrder;
use Shleif\UserBundle\Entity\Buyer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends FOSRestController
{
    /**
     * @Rest\Get("/get-managers", name="get_managers")
     */
    public function getManagersAction()
    {
        return $this->getDoctrine()
            ->getRepository("UserBundle:User")
            ->findByRole("ROLE_MANAGER")
            ->getResult();
    }

    /**
     * @Rest\Get("/get-statuses")
     */
    public function getStatusesAction()
    {
        return $this->getDoctrine()->getRepository("CatalogBundle:Status")->findAll();
    }

    /**
     * @Rest\Get("/get-couriers")
     */
    public function getCouriersAction()
    {
        return $this->getDoctrine()->getRepository("UserBundle:Courier")->findAll();
    }

    /**
     * @Rest\Get("/get-payments")
     */
    public function getPaymentsAction()
    {
        return $this->getDoctrine()->getRepository("CatalogBundle:Payment")->findAll();
    }

    /**
     * @Rest\Get("/get-from-addresses")
     */
    public function getFromAddressesAction()
    {
        return $this->getDoctrine()->getRepository("CatalogBundle:FromAddress")->findAll();
    }

    /**
     * @Rest\Get("/get-buyers", name="get_buyers")
     * @return array|\Shleif\UserBundle\Entity\Buyer[]
     */
    public function getBuyersAction()
    {
        $buyers = $this->getDoctrine()
            ->getRepository('UserBundle:Buyer')
            ->findAll();

        return $buyers;
    }

    /**
     * @Rest\Get("/get-products", name="get-products")
     */
    public function getProductsAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('CatalogBundle:Product')
            ->findAll();

        return $products;
    }

    /**
     * @Rest\Post("/get-orders", name="get_orders")
     *
     * @param Request $request
     * @return mixed
     */
    public function getOrdersAction(Request $request)
    {
        $filters = $request->get('filters');

        $orders = $this->getDoctrine()
            ->getRepository("CatalogBundle:CatalogOrder")
            ->findWithPaginate($request->get('page'), $request->get('perPage'), $filters);

        return $orders;
    }

    /**
     * @Rest\Post("/set-discount", name="set_discount")
     * @param Request $request
     */
    public function setDiscountAction(Request $request)
    {
        $value = $request->get('val');

        $buyer = $this->getDoctrine()
            ->getRepository('UserBundle:Buyer')
            ->findOneBy(['id' => $request->get('id')]);

        $buyer->setDiscount($value);
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * @Rest\Get("/{id}/get-buyer-discount", name="get_buyer_discount")
     *
     * @param Buyer $buyer
     *
     * @return integer
     */
    public function getBuyerDiscountAction(Buyer $buyer)
    {
        return $buyer->getDiscount();
    }

    /**
     * @Rest\Post("/new-order", name="api_new_order")
     *
     * @param Request $request
     * @return Response
     */
    public function addOrderAction(Request $request)
    {
        $data = $request->get('order');
        $products = $data['products'];
        unset($data['products']);

        $buyer = $this->container->get('buyer.service')->createBuyer($data['buyer']);
        unset($data['buyer']);

        $order = $this->container->get('catalog.service')->createOrder($data, $buyer);

        $this->container->get('order_product.service')->linkOrderAndProducts($order, $products);

        return new Response('order was created', Response::HTTP_CREATED);
    }

    /**
     * @Rest\Get("/orders/{id}", name="get_order")
     *
     * @param CatalogOrder $order
     *
     * @return CatalogOrder
     */
    public function getOrderByIdAction(CatalogOrder $order)
    {
        return $order;
    }

    /**
     * @Rest\Get("/instruction", name="get_instruction")
     *
     * @return null|object|\Shleif\CatalogBundle\Entity\Instruction
     */
    public function getInstructionAction()
    {
        $instruction = $this->getDoctrine()->getRepository('CatalogBundle:Instruction')->findOneBy([]);

        return $instruction;
    }
}