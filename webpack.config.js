"use strict";

module.exports = {
    entry: './web/js/vuejs/app.js',
    output: {
        filename: './web/js/app.js'
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    }
};