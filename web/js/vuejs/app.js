import Vue from 'vue'

window.$ = require('jquery');
window.axios = require('axios');

Vue.component('order-filter', require('./Components/OrderFilter.vue'));
Vue.component('new-order', require('./Components/NewOrder.vue'));
Vue.component('yamap', require('./Components/Map.vue'));
Vue.component('manager', require('./Components/Managers/Autocomplite.vue'));
Vue.component('courier', require('./Components/Couriers/Autocomplite.vue'));

const app = new Vue({
    el: '#app',
    components: {
        "vue-datetime-picker": require("vue-datetime-picker")
    },
});