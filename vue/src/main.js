import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource);

window.$ = window.jQuery = require('jquery');
window.moment = require('moment');
window.moment.locale('ru');
require('eonasdan-bootstrap-datetimepicker');
require('jquery-ui-dist/jquery-ui.min.js');

import NewOrder from './components/NewOrder.vue'
import OrderFilter from './components/OrderFilter.vue'

new Vue({
  el: '#app',
  components: {
      NewOrder,
      OrderFilter
  }
});
